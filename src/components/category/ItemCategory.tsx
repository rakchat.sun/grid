import React from "react";

// Material-ui
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  conCategory: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    marginLeft: "2.5%",
    width: "96%",
    height: "96px",
    backgroundColor: "black",
    borderRadius: "25px",
    color: "white",
    [theme.breakpoints.down("xs")]: {
      marginLeft: "2%",
      marginBottom: "6%",
    },
  },

  txtCategory: {
    fontSize: "27px",
    alignSelf: "center",
    [theme.breakpoints.down("sm")]: {
      fontSize: "21px",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "23px",
    },
  },
}));

export default function ItemCategory(props: any) {
  const classes = useStyles();
  return (
    <Grid item lg={3} md={3} sm={3} xs={6}>
      <Box
        className={classes.conCategory}
        // onClick={() => alert(props.data.category_name)}
      >
        <Box className={classes.txtCategory}>{props.data.category_name}</Box>
      </Box>
    </Grid>
  );
}
