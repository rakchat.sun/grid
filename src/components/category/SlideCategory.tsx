import React from "react";

// Material-ui
import { makeStyles } from "@material-ui/core/styles";
// import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

// Screen
import ItemCategory from "./ItemCategory";

// CSS
// import "../styles/styleTopten.css";

// DATA
import DataCategory from "./DataCategory";

const useStyles = makeStyles({
  containerBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    flexWrap: "wrap",
    // Add
    width: "100%",
    margin: "auto",
    textTransform: "uppercase",
  },
});

const useStyles2 = makeStyles((theme) => ({
  root: {
    paddingLeft: "50px",
    paddingRight: "50px",
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "0px",
      paddingRight: "0px",
    },
  },
}));

export default function SlideCategory(props: any) {
  const classes = useStyles();
  const classes2 = useStyles2();
  const dataService = DataCategory.result.data.category_list;
  const itemList = [];
  let start = props.start - 4;
  let sed = 4 - props.sed;

  switch (sed) {
    case 1:
      for (let i = start; i < start + 1; i++) {
        itemList.push(<ItemCategory key={i} data={dataService[i]} />);
      }
      break;
    case 2:
      for (let i = start; i < start + 2; i++) {
        itemList.push(<ItemCategory key={i} data={dataService[i]} />);
      }
      break;
    case 3:
      for (let i = start; i < start + 3; i++) {
        itemList.push(<ItemCategory key={i} data={dataService[i]} />);
      }
      break;
    default:
      for (let i = start; i < start + 4; i++) {
        itemList.push(<ItemCategory key={i} data={dataService[i]} />);
      }
  }
  return (
    <Grid container direction="column">
      <Container
        classes={{ root: classes2.root }}
        className={classes.containerBox}
      >
        {itemList}
      </Container>
    </Grid>
  );
}
