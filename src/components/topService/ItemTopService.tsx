import React from "react";

// Material-ui
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  conService: {
    width: "100%",
    height: "11rem",
    // backgroundColor: "red",
    [theme.breakpoints.down("sm")]: {
      height: "9rem",
    },
  },

  conData: {
    position: "relative",
    width: "100%",
    height: "74%",
  },

  content: {
    display: "flex",
    flexDirection: "column",
    position: "relative",
    fontSize: "1.125rem",
    wordWrap: "break-word",
    height: "100%",
    marginLeft: "10.5rem",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "8.5rem",
    },
  },

  circleImage: {
    width: "180px",
    height: "180px",
    borderRadius: "50%",
    position: "absolute",
    [theme.breakpoints.down("sm")]: {
      width: "140px",
      height: "140px",
    },
  },

  boxContent: {
    backgroundColor: "#fabd23",
    width: "90%",
    left: "5.5%",
    top: "17%",
    height: "102%",
    position: "relative",
    borderRadius: "25px",
    boxShadow: "0px 2px 5px grey",

    [theme.breakpoints.down("sm")]: {
      height: "95%",
    },
  },

  txtCategoryName: {
    color: "#7e7e7e",
    fontWeight: "bold",
    fontSize: "1.3rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1rem",
    },
  },

  txtServicePrice: {
    fontWeight: "bold",
    fontSize: "1.8rem",
    marginTop: "4px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.4rem",
    },
    [theme.breakpoints.down("xs")]: {
      marginTop: "7px",
      fontSize: "1.2rem",
    },
  },

  btnPayNow: {
    color: "white",
    padding: "5px 34px 5px 34px",
    backgroundColor: "black",
    borderRadius: "25px",
    bottom: "13%",
    right: "6%",
    fontWeight: "bold",
    fontSize: "1.1rem",
    position: "absolute",
    [theme.breakpoints.down("sm")]: {
      padding: "3px 8px 3px 8px",
      bottom: "8%",
      fontSize: "0.8rem",
    },
    [theme.breakpoints.down("xs")]: {
      padding: "4px 16px 4px 16px",
      bottom: "12%",
      fontSize: "0.7rem",
    },
  },
}));

export default function ItemTopService(props: any) {
  const classes = useStyles();
  return (
    <Grid item lg={6} md={6} sm={6} xs={12}>
      <Box mb={2} className={classes.conService}>
        <Box className={classes.conData}>
          <Box ml={1}>
            <img
              style={{ zIndex: 1 }}
              className={classes.circleImage}
              src="https://jaksestudio.com/wp-content/uploads/2020/03/JAKS-estudio-Womens-Haircut.png"
              alt="new"
            />
          </Box>
          <Box className={classes.boxContent}>
            <Box className={classes.content}>
              <Box mt={1} className="txtServiceName">
                {props.data.service_name}
              </Box>
              <Box className={classes.txtCategoryName}>
                ({props.data.category_name})
              </Box>
              <Box className={classes.txtServicePrice}>
                {props.data.service_price} ฿
              </Box>
              <button className={classes.btnPayNow}>Pay Now</button>
            </Box>
          </Box>
        </Box>
      </Box>
    </Grid>
  );
}
