import React from "react";

// Material-ui
import { makeStyles } from "@material-ui/core/styles";
// import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

// Screen
import ItemAllService from "./ItemAllService";

// CSS
// import "../styles/styleTopten.css";

// DATA
import DataAllService from "./DataAllService";

const useStyles = makeStyles({
  containerBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
});

const useStyles2 = makeStyles((theme) => ({
  root: {
    paddingLeft: "50px",
    paddingRight: "50px",
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "0px",
      paddingRight: "0px",
    },
  },
}));

export default function SlideAllService(props: any) {
  const classes = useStyles();
  const classes2 = useStyles2();
  const dataService = DataAllService.result.data.all_service_item;
  const itemList = [];
  let start = props.start - 8;
  let sed = 8 - props.sed;
  console.log("item sed: " + sed);

  switch (sed) {
    case 1:
      for (let i = start; i < start + 1; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    case 2:
      for (let i = start; i < start + 2; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    case 3:
      for (let i = start; i < start + 3; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    case 4:
      for (let i = start; i < start + 4; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    case 5:
      for (let i = start; i < start + 5; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    case 6:
      for (let i = start; i < start + 6; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    case 7:
      for (let i = start; i < start + 7; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
      break;
    default:
      for (let i = start; i < start + 8; i++) {
        itemList.push(<ItemAllService key={i} data={dataService[i]} />);
      }
  }
  return (
    <Grid container direction="column">
      <Container
        classes={{ root: classes2.root }}
        className={classes.containerBox}
      >
        {itemList}
      </Container>
    </Grid>
  );
}
