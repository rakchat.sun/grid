import React from "react";

// Material-ui
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";

// CSS
import "swiper/swiper-bundle.css";
import "./App.css";
import "./components/category/ItemCategory.css";
import "./components/topService/ItemTopService.css";
import "./components/allService/ItemAllService.css";

// Category
import DataCategory from "./components/category/DataCategory";
import SlideCategory from "./components/category/SlideCategory";

// TopService
import DataTopService from "./components/topService/DataTopService";
import SlideTopService from "./components/topService/SlideTopService";

// AllService
import DataAllService from "./components/allService/DataAllService";
import SlideAllService from "./components/allService/SlideAllService";

// Swiper
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, {
  Navigation,
  Pagination,
  Controller,
  Thumbs,
  EffectFade,
} from "swiper";
SwiperCore.use([Navigation, Pagination, Controller, Thumbs, EffectFade]);

const useStyles = makeStyles((theme) => ({
  imgBanner: {
    width: "100%",
    height: "200px",
    borderRadius: "24px",
    [theme.breakpoints.down("xs")]: {
      height: "150px",
    },
  },
  txtTopService: {
    fontSize: "28px",
    fontWeight: 600,
    marginBottom: "16px",
  },
  txtAllService: {
    fontSize: "28px",
    fontWeight: 600,
    marginBottom: "16px",
  },
}));

const useStyles2 = makeStyles((theme) => ({
  txtRoot: {
    paddingLeft: "80px",
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "10px",
    },
  },
  imgRoot: {
    paddingLeft: "55px",
    paddingRight: "55px",
    [theme.breakpoints.down("xs")]: {
      paddingLeft: "2px",
      paddingRight: "2px",
    },
    [theme.breakpoints.up("lg")]: {
      paddingLeft: "100px",
      paddingRight: "100px",
    },
    [theme.breakpoints.up("xl")]: {
      paddingLeft: "380px",
      paddingRight: "380px",
    },
  },
}));

function App() {
  const classes = useStyles();
  const classes2 = useStyles2();

  // Start slidesCategory
  const slideCategory = [];
  let sizeDataCategory = DataCategory.result.data.category_list.length;
  let startCategory = 0;
  let sedCategory = 0;

  for (let i = 0; i < sizeDataCategory / 4; i += 1) {
    startCategory = startCategory + 4;
    sedCategory = startCategory - sizeDataCategory;
    slideCategory.push(
      <SwiperSlide key={`slide-${i}`} tag="li">
        <SlideCategory start={startCategory} sed={sedCategory} />
      </SwiperSlide>
    );
  }
  // End slidesCategory

  // Start slidesTopService
  const slidesTopService = [];
  let sizeData = DataTopService.result.data.all_service_item.length;
  let start = 0;
  let sed = 0;

  for (let i = 0; i < sizeData / 4; i += 1) {
    start = start + 4;
    sed = start - sizeData;
    slidesTopService.push(
      <SwiperSlide key={i} tag="li">
        <SlideTopService start={start} sed={sed} />
      </SwiperSlide>
    );
  }
  // End slidesTopService

  // Start slidesAllService
  const slidesAllService = [];
  let sizeDataAll = DataAllService.result.data.all_service_item.length;
  let startAll = 0;
  let sedAll = 0;

  for (let i = 0; i < sizeDataAll / 8; i += 1) {
    startAll = startAll + 8;
    sedAll = startAll - sizeDataAll;
    slidesAllService.push(
      <SwiperSlide key={i} tag="li">
        <SlideAllService start={startAll} sed={sedAll} />
      </SwiperSlide>
    );
  }
  // End slidesAllService

  // Start Image Banner
  var imgBanner = (
    <img
      className={classes.imgBanner}
      src={require("./assets/images/banner.png").default}
      alt="banner"
    />
  );
  // End Image Banner

  return (
    <Box>
      <Box m={3} />
      <Box className={classes2.imgRoot}>{imgBanner}</Box>
      <Box m={1} />
      <Swiper
        id="category"
        tag="section"
        // effect={"fade"}
        navigation
      >
        {slideCategory}
      </Swiper>
      <Box m={3} />
      <Container
        classes={{ root: classes2.txtRoot }}
        className={classes.txtTopService}
      >
        Top 10 Service
      </Container>
      <Swiper
        id="topService"
        tag="section"
        // effect={"fade"}
        navigation
        pagination={{
          clickable: true,
          dynamicBullets: true,
        }}
      >
        {slidesTopService}
      </Swiper>
      <Box m={3} />
      <Container
        classes={{ root: classes2.txtRoot }}
        className={classes.txtAllService}
      >
        All Service
      </Container>
      <Swiper
        id="allService"
        tag="section"
        // effect={"fade"}
        navigation
        pagination={{
          clickable: true,
          dynamicBullets: true,
        }}
      >
        {slidesAllService}
      </Swiper>
    </Box>
  );
}

export default App;
